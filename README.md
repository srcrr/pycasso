Pycasso
=======

[![image](https://img.shields.io/pypi/v/pycasso.svg)](https://pypi.python.org/pypi/pycasso)

[![image](https://img.shields.io/travis/src-r-r/pycasso.svg)](https://travis-ci.com/src-r-r/pycasso)

[![Documentation Status](https://readthedocs.org/projects/pycasso/badge/?version=latest)](https://pycasso.readthedocs.io/en/latest/?badge=latest)

Generate (somewhat) beautiful abstract art for placeholder images.

Some examples of images Pycasso generates:

![cover-1](./docs/images/cover-1.png)
![cover-2](./docs/images/cover-2.png)
![cover-3](./docs/images/cover-3.png)
![cover-4](./docs/images/cover-4.png)
![cover-5](./docs/images/cover-5.png)



-   Free software: MIT license
-   Documentation: <https://pycasso.readthedocs.io>.

Why Pycasso?
------------

Let\'s say you\'re working on a Django website, or another Python
project that requires the use of images. You can either download a bunch
of placeholder images off the Internet, but that takes a lot of time,
and sites like LoremPicsum tend to have a lot of duplicates.

Pycasso removes all of the brain work and allows you to generate
(somewhat) beautiful placeholder images for your project.

Pycoasso uses the Pillow (PIL) library for drawing, so the only
requirement is Python!

Usage
-----

### Library

To use Pycasso in your unit tests, just import the Pycasso class

```python
>>> from pycasso.pycasso import Pycasso
>>> Pycasso(200, 200, background="black").have_fun().layers[0].show()
apply boxblur
Apply add_modulo
apply gausian blur
apply boxblur
Apply composite
apply sharpen
```
![initial_example](./docs/images/initial-example.png)

### Command Line

Want to create an image on the fly? You can use pycasso on the command line:

```bash
pycasso generate 250 200 ./docs/images/cli-example.png -l 5 -i 10 -s 24 -b black
```
![initial_example](./docs/images/cli-example.png)

Features
--------

-   TODO

Credits
-------

This package was created with
[Cookiecutter](https://github.com/audreyr/cookiecutter) and the
[audreyr/cookiecutter-pypackage](https://github.com/audreyr/cookiecutter-pypackage)
project template.
