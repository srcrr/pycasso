"""Console script for pycasso."""
import sys
import click
from .pycasso import generate_random_palette, Pycasso
from PIL import ImagePalette


@click.group()
def cli():
    pass


@cli.command()
@click.argument("width", type=int)
@click.argument("height", type=int)
@click.argument("out", type=click.File("wb+"))
@click.option(
    "-p",
    '--palette',
    help=
    "Path to the color palette to use for the image. If omitted a randomly-generated palette will be used.",
    type=click.File("rb"))
@click.option("-l",
              "--layers",
              type=int,
              help="Number of layers to generate",
              default=5)
@click.option("-s",
              "--shapes",
              type=int,
              default=10,
              help="Number of shapes per layer to generate")
@click.option("-i",
              "--iterations",
              type=int,
              default=7,
              help="Number of times each layer will be distorted.")
@click.option("-b",
              "--background",
              type=click.Choice(Pycasso.BG_CHOICES),
              default=Pycasso.BG_TRANSPARENT,
              help="Number of times each layer will be distorted.")
def generate(width, height, out, palette, layers, shapes, iterations,
             background):
    painting = Pycasso(width, height, background=background)
    painting.have_fun(layers, shapes, iterations)
    result = painting.layers[0]
    result.save(out)


def main():
    cli()


if __name__ == "__main__":
    cli()