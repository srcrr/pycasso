"""Main module."""
from PIL import Image, ImageDraw, ImageColor as Color, ImageChops, ImageFilter, ImagePalette, ImageEnhance
import random
import numpy as np
import sys

WIDTH = 200
HEIGHT = 200
OUT_PATH = "./example.png"

N_CIRCLES = 10


def rrange(a, b=None):
    if not b:
        return range(0, random.randint(0, a))
    return range(0, random.randint(a, b))


def random_point(width, height, guard=1.0):
    return (random.randint(0,
                           width * guard), random.randint(0, height * guard))


def random_cv():
    return random.randint(0, 255)


def random_rgba(alpha=255):
    return f"rgba({','.join([random_cv() for i in range(0, 3)])},{alpha or random_cv()})"


def random_rgb(alpha=255):
    return f"rgb({','.join([random_cv() for i in range(0, 3)])})"


def generate_random_palette(n_colors=256, mode="RGBA"):
    p = ImagePalette.ImagePalette(mode=mode)
    for i in range(0, n_colors):
        r = random_cv()
        g = random_cv()
        b = random_cv()
        a = random_cv()
        if "A" in mode:
            p.getcolor((r, g, b, a))
        else:
            p.getcolor((r, g, b))
    return p


class PycassoLayer(object):
    def __init__(self,
                 width,
                 height,
                 x=0,
                 y=0,
                 color_palate=None,
                 zoom=1.5,
                 background=255):
        self.width = width
        self.height = height
        self.x = x
        self.y = y
        self.color_palate = color_palate or generate_random_palette()
        self.zoom = zoom
        background = background or Color.getrgb("rgba(0, 0, 0, 0)")
        self._img = Image.new(mode=self.color_palate.mode,
                              size=(self.width, self.height))
        self._draw = ImageDraw.Draw(self._img)
        # self._draw.palette = self.color_palate
        # self._img.palette = self.color_palate
        # super(PycassoLayer, self).__init__(self._img)

    @property
    def image(self):
        # self._img.palette = self.color_palate
        return self._img

    @property
    def _random_x(self):
        return random.randint(0, int(self.width * self.zoom))

    @property
    def _random_y(self):
        return random.randint(0, int(self.height * self.zoom))

    @property
    def _random_bbox(self):
        return [self._random_x, self._random_y, self._random_x, self._random_y]

    @property
    def _random_angle(self):
        return random.randint(0, 360)

    def _random_alpha_fill(self, a="%"):
        values = list(self.color_palate.colors.keys())
        chosen = [str(i) for i in random.choice(values)]
        return Color.getrgb(self.color_palate.mode + "(" + ",".join(chosen) +
                            ")")

    @property
    def _random_fill(self):
        return self._random_alpha_fill()

    @property
    def _random_outline(self):
        return self._random_fill

    def _random_alpha_outline(self, a='%'):
        return self._random_outline

    @property
    def _random_width(self):
        return random.randint(1, 12)

    def _random_point_sequence(self, a=4, b=19):
        return [(self._random_x, self._random_y) for i in rrange(a, b)]

    def arc(self, xy=None, start=None, end=None, fill=None, width=None):
        xy = xy or self._random_bbox
        start = start or self._random_angle
        end = end or self._random_angle
        fill = fill or self._random_alpha_fill()
        width = width or self._random_width
        self._draw.arc(xy, start, end, fill, width)
        return self

    def chord(self, xy=None, start=None, end=None, fill=None, width=None):
        xy = xy or self._random_bbox
        start = start or self._random_angle
        end = end or self._random_angle
        fill = fill or self._random_alpha_fill()
        width = width or self._random_width
        self._draw.chord(xy, start, end, fill, width)
        return self

    def ellipse(self, xy=None, fill=None, outline=None, width=None):
        xy = xy or self._random_bbox
        fill = fill or self._random_alpha_fill()
        outline = outline or self._random_alpha_outline()
        width = width or self._random_width
        self._draw.ellipse(xy, fill, outline, width)
        return self

    def line(self, xy=None, fill=None, width=None):
        xy = xy or self._random_bbox
        fill = fill or self._random_alpha_fill()
        width = width or self.width
        self._draw.line(xy, fill, width)
        return self

    def pieslice(self, xy=None, start=None, end=None, fill=None, width=None):
        xy = xy or self._random_bbox
        start = start or self._random_angle
        end = end or self._random_angle
        fill = fill or self._random_alpha_fill()
        width = width or self._random_width
        self._draw.pieslice(xy, start, end, fill, width)
        return self

    def polygon(self, xy=None, fill=None, outline=None):
        xy = xy or self._random_point_sequence()
        fill = fill or self._random_alpha_fill()
        outline = outline or self._random_alpha_outline()
        self._draw.polygon(xy, fill, outline)
        return self

    def mandelbrot(self, size=None, extent=None, quality=None):
        size = size or (self.width, self.height)
        extent = extent or self._random_bbox
        quality = quality or 3
        mb = Image.effect_mandelbrot(size,
                                     (0, 0, self.width * 4, self.height * 4),
                                     quality=quality)
        mb = mb.convert("RGBA")
        self._img.alpha_composite(mb)
        return self

    def generate(self, iterations=None):
        bank = [
            self.arc,
            self.chord,
            self.ellipse,
            self.line,
            self.pieslice,
            # self.mandelbrot,
            self.polygon,
        ]
        iterations = iterations or len(bank)
        for i in range(0, iterations):
            random.choice(bank)()
        return self


class Pycasso(object):
    BG_BLACK = "black"
    BG_WHITE = "white"
    BG_TRANSPARENT = "transparent"
    BG_CHOICES = (BG_BLACK, BG_WHITE, BG_TRANSPARENT)

    def __init__(self,
                 width,
                 height,
                 color_palate=None,
                 background=BG_TRANSPARENT):
        self.layers = []
        self.width = width
        self.height = height
        self.background = background
        self.color_palate = color_palate or generate_random_palette()

    def generate_layer(self, n_shapes=None):
        self.layers.append(
            PycassoLayer(self.width,
                         self.height,
                         color_palate=self.color_palate).generate(n_shapes))
        return self

    def _generate_mask(self):
        return PycassoLayer(self.width,
                            self.height,
                            0,
                            0,
                            color_palate=self.color_palate)

    def layer_chop(self, chop, **chop_kwargs):
        if not len(self.layers):
            raise AttributeError("Pycasso needs at least 1 layer")
        if len(self.layers) == 1:
            return self
        if chop in ("multiply", "divide", "subtract"):
            # These result in a black image
            return self
        # if "mask" in chop_kwargs:
        #     chop_kwargs["mask"].palette = self.color_palate
        l1 = self.layers.pop()
        l2 = self.layers.pop()
        if isinstance(l1, PycassoLayer):
            l1 = l1.image
        if isinstance(l2, PycassoLayer):
            l2 = l2.image
        # l1.palette = self.color_palate
        # l2.palette = self.color_palate
        print(f"Apply {chop}")
        chp = getattr(ImageChops, chop)
        # import ipdb; ipdb.set_trace()
        comb = chp(l1, l2, **chop_kwargs)
        # comb.palette = self.color_palate
        self.layers.append(comb)
        return self

    def composite(self):
        return self.layer_chop("composite", mask=self._generate_mask().image)

    def blend(self):
        return self.layer_chop("blend", mask=self._generate_mask().image)

    def multiply(self):
        return self.layer_chop("multiply")

    def subtract(self):
        return self.layer_chop("subtract")

    def subtract_modulo(self):
        return self.layer_chop("subtract_modulo")

    def add_modulo(self):
        return self.layer_chop("add_modulo")

    def blur(self, radius=None, force=True):
        print("apply gausian blur")
        if not force and len(self.layers) == 1:
            return self
        radius = radius or random.randint(0, min([self.width, self.height, 30
                                                  ]))

        l1 = self.layers.pop()
        if isinstance(l1, PycassoLayer):
            l1 = l1.image

        # l1.palette = self.color_palate
        l1 = l1.filter(ImageFilter.GaussianBlur(radius))
        return self.layers.append(l1)

    def boxblur(self, radius=None, force=True):
        print("apply boxblur")
        if not force and len(self.layers) == 1:
            return self
        radius = radius or random.randint(0, min([self.width, self.height, 30
                                                  ]))

        l1 = self.layers.pop()
        if isinstance(l1, PycassoLayer):
            l1 = l1.image

        l1 = l1.filter(ImageFilter.BoxBlur(radius))
        return self.layers.append(l1)

    def sharpen(self, factor=0.25):
        print("apply sharpen")
        l1 = self.layers.pop()
        if isinstance(l1, PycassoLayer):
            l1 = l1.image
        enhancer = ImageEnhance.Sharpness(l1)
        l1 = enhancer.enhance(factor)
        self.layers.append(l1)

    def abstractify(self, iterations=None):
        bank = [
            self.composite,
            self.add_modulo,
            self.multiply,
            self.subtract,
            self.subtract_modulo,
            self.blur,
            self.boxblur,
        ]
        iterations = iterations or len(bank)
        while len(self.layers) > 1:
            random.choice(bank)()
        # Add a blur and sharpen to make it look "real"
        # self.blur(force=True)
        self.sharpen()

    def have_fun(self,
                 layers=3,
                 n_shapes_per_layer=5,
                 abstractify_iterations=None):
        for i in range(0, layers):
            self.generate_layer(n_shapes_per_layer)
        while len(self.layers) >= 2:
            self.abstractify(abstractify_iterations)
        bg_layer = None
        l = self.layers[0]
        if self.background == self.BG_WHITE:
            bg_layer = Image.new(l.mode,
                                 (self.width, self.height),
                                 color=Color.getrgb("rgba(255,255,255,255)"))
        if self.background == self.BG_BLACK:
            bg_layer = Image.new(l.mode,
                                 (self.width, self.height),
                                 color=Color.getrgb("rgba(0,0,0,255)"))
        if bg_layer:
            self.layers.append(
                Image.alpha_composite(bg_layer, self.layers.pop()))
        return self
